<?php
	session_start();
	 
	include("connexiondb.php"); 
	 
	if(isset($_SESSION['id'])) {
	   	$req_user = $dbh->prepare('SELECT * FROM user WHERE id = ?');
    	$req_user->execute(array($_SESSION['id']));
    	$user_infos = $req_user->fetch();
	   	if(isset($_POST['newpseudo']) AND !empty($_POST['newpseudo']) AND $_POST['newpseudo'] != $user_infos['pseudo']) {
		    $newpseudo = htmlspecialchars($_POST['newpseudo']);
		    $insertpseudo = $dbh->prepare("UPDATE user SET pseudo = ? WHERE id = ?");
		    $insertpseudo->execute(array($newpseudo, $_SESSION['id']));
		    header('Location: userprofil.php?id='.$_SESSION['id']);
	   	}
	   	if(isset($_POST['new_mail']) AND !empty($_POST['new_mail']) AND $_POST['new_mail'] != $user_infos['mail']) {
	        $new_mail = htmlspecialchars($_POST['new_mail']);
	        $insertmail = $dbh->prepare("UPDATE user SET email = ? WHERE id = ?");
	        $insertmail->execute(array($new_mail, $_SESSION['id']));
	        header('Location: userprofil.php?id='.$_SESSION['id']);
	    }
	    if(isset($_POST['new_mdp']) AND !empty($_POST['new_mdp']) AND isset($_POST['new_mdp2']) AND !empty($_POST['new_mdp2'])) {
	    	$new_mdp = $_POST['new_mdp'];
	        $new_mdp2 = $_POST['new_mdp2'];
	        if($new_mdp == $new_mdp2) {
	        	$new_mdp = sha1($new_mdp);
	            $insert_mdp = $dbh->prepare("UPDATE user SET password = ? WHERE id = ?");
	            $insert_mdp->execute(array($new_mdp, $_SESSION['id']));
	            header('Location: userprofil.php?id='.$_SESSION['id']);
	        }else{
	            $erreur = "Vos deux mdp ne correspondent pas !";
	        }  
	    }

	    if(isset($_FILES['img_profile']) AND !empty($_FILES['img_profile']['name'])){
	    	/*Valeur en bits de 10 Mo*/
	   		$taille_max =  10485760;
	   		$extension = array('jpg', 'jpeg', 'gif', 'png');
	   		if($_FILES['img_profile']['size'] <= $taille_max){
	   			/* Met l'extension en minuscules, permet d'ignorer le 1er caractere et en selectionnant ce qui vient apres le point*/
	   			$extension_img = strtolower(substr(strrchr($_FILES['img_profile']['name'], '.'), 1));
	   			/* Test si l'extension est valide */
	   			if(in_array($extension_img, $extension)){
	   				/* Récupère le chemin d'acces au repertoire de stockage des images d'utilisateurs*/
	   				$path = "image_profiles/".$_SESSION['id'].".".$extension_img;
	   				/* Envoie l'image dans le répertoire indiquée dans la variable de récupération du répertoire */ 
	   				$move = move_uploaded_file($_FILES['img_profile']['tmp_name'], $path);
	   				if ($move) {
	   					$req_update_img_profile = $dbh->prepare('UPDATE user SET img_profile = :img_profile WHERE id = :id');
	   					$req_update_img_profile->execute(array(
	   															'img_profile' => $_SESSION['id'].".".$extension_img,
	   															'id' => $_SESSION['id']
	   														));
	   					header('Location: userprofil.php?id='.$_SESSION['id']);
	   				}else{
	   					$erreur = "erreur a l'inportation";
	   				}
	   			}else{
	   				$erreur = "photo pas au bon format";
	   			}
	   		}else{
	   			$erreur = "photo trop lourde";
	   		}
		}   		
	}
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Compos builder</title> 
        <link rel="stylesheet" href="index.css"> 
    </head>
  
    <body id="main">
    	<?php include("sidenav.php"); ?>
    	<h1>Mon profil</h1><br /><br />

        <div id="edit_profile">
	        <h2>Editer mon profil</h2>
	        <?php
	            		if(!empty($user_infos['img_profile'])){?>
	            			<img src="image_profiles/<?php echo $user_infos['img_profile']; ?>" id="user_img">
	            		<?php
	            		}
	            	?>
	            <form id="edit_form" method="POST" action="" enctype="multipart/form-data">
	            	</br></br>
	                <label>Pseudo :</label>
	                <input type="text" name="newpseudo" placeholder="Pseudo" id="newpseudo" value="<?php echo $user_infos['pseudo']; ?>" /><br /><br />
	                <label>Mail :</label>
	                <input type="text" name="new_mail" placeholder="Mail" id="new_mail" value="<?php echo $user_infos['email']; ?>" /><br /><br />
	                <label>Mot de passe :</label>
	                <input type="password" name="new_mdp" placeholder="Mot de passe" id="new_mdp"/><br /><br />
	                <label>Confirmation du mot de passe :</label>
	                <input type="password" name="new_mdp2" placeholder="Confirmation du mot de passe" id="new_mdp2"/><br /><br />
	                <label>Image de profil (Max 10Mo aux formats jpg, jpeg, gif ou png) :</label><br /><br />
	                <input type="file" name="img_profile"><br /><br />
	                <input type="submit" value="Mettre à jour mon profil !" />
	                <?php /*Affiche le message d'erreur si il y en a un*/ 
	            		if(isset($erreur)){ 
	            			echo '<font color="red">'.$erreur."</font>"; 
	            		} 
	            	?>
	            </form> 
	            
        </div>
      	<script src="index.js"></script>
   </body>
</html>
