<?php
    session_start();

    include("connexiondb.php"); 
     
    if(isset($_GET['id']) AND $_GET['id'] == $_SESSION['id']) {
        /*Converti la valeur en int*/ 
        $get_id = intval($_GET['id']);
        $req_user = $dbh->prepare('SELECT * FROM user WHERE id = ?');
        $req_user->execute(array($get_id));
        $user_infos = $req_user->fetch();

        $req_supp = $dbh->prepare('SELECT personnage.`id`, name FROM personnage JOIN composition on personnage.`id` = composition.`supp` WHERE composition.`id` = ?');
        $req_adc = $dbh->prepare('SELECT personnage.`id`, name FROM personnage JOIN composition on personnage.`id` = composition.`adc` WHERE composition.`id` = ?');
        $req_mid = $dbh->prepare('SELECT personnage.`id`, name FROM personnage JOIN composition on personnage.`id` = composition.`mid` WHERE composition.`id` = ?');
        $req_jungle = $dbh->prepare('SELECT personnage.`id`, name FROM personnage JOIN composition on personnage.`id` = composition.`jungle` WHERE composition.`id` = ?');
        $req_top = $dbh->prepare('SELECT personnage.`id`, name FROM personnage JOIN composition on personnage.`id` = composition.`top` WHERE composition.`id` = ?');

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Compos builder</title> 
        <link rel="stylesheet" href="index.css"> 
    </head>
  
    <body id="main">
    
        <h1>Compos builder</h1>

        <?php include("sidenav.php"); ?>

        <table id="profil_access_user"  align="right">
            <tr>
                <td>
                    <?php echo "<a href=\"userprofil.php?id=".$_SESSION['id']."\">
                                    <img src='image_profiles/".$user_infos_img['img_profile']."'  style='width:60px; height:60px;'>
                              </a>";
                    ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo "<a href=\"userprofil.php?id=".$_SESSION['id']."\">Editer mon profil</a>"; ?>
                </td>
            </tr>
            <tr>
                <td>
                   <a href="deconnexion.php">Se déconnecter</a>
                </td>
            </tr>          
        </table>

        <h2>Mes compos</h2>

        <?php
            if(isset($_GET['id']) AND $_GET['id'] == $_SESSION['id']){
                foreach($dbh->query('SELECT id, user_id from composition WHERE user_id = "'.$get_id.'"') as $row
                    ) {
                        $req_supp->execute(array($row['id']));
                        $supp_name = $req_supp->fetch();

                        $req_adc->execute(array($row['id']));
                        $adc_name = $req_adc->fetch();

                        $req_mid->execute(array($row['id']));
                        $mid_name = $req_mid->fetch();

                        $req_jungle->execute(array($row['id']));
                        $jungle_name = $req_jungle->fetch();

                        $req_top->execute(array($row['id']));
                        $top_name = $req_top->fetch();
                        echo  " <div>
                                    <table> 
                                        <tr>
                                            <td>
                                                <img src='image_champs/".$supp_name['name'].".jpg' class='compos_champs'>
                                            </td>
                                            <td>
                                                <img src='image_champs/".$adc_name['name'].".jpg' class='compos_champs'>
                                            </td>
                                            <td>
                                                <img src='image_champs/".$mid_name['name'].".jpg' class='compos_champs'>
                                            </td>
                                            <td>
                                                <img src='image_champs/".$jungle_name['name'].".jpg' class='compos_champs'>
                                            </td>
                                            <td>
                                                <img src='image_champs/".$top_name['name'].".jpg' class='compos_champs'>
                                            </td>
                                            <td>
                                                <a href=\"delete_compo.php?id_comp=".$row['id']."&id=".$_SESSION['id']."\">
                                                    <img src='delete.png' id='delete".$row['id']."' class='edit_icons'>
                                                </a>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td id='supp'>
                                                ".$supp_name['name']."
                                            </td>
                                            <td id='adc'>
                                                ".$adc_name['name']."
                                            </td>
                                            <td id='mid'>
                                                ".$mid_name['name']."
                                            </td>
                                            <td id='jungle'>
                                                ".$jungle_name['name']."
                                            </td>
                                            <td id='top'>
                                                ".$top_name['name']."
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id='conseil'>
                                    <table class='conseils_table'>";
                                        $supp = $supp_name['id'];
                                        $adc = $adc_name['id'];
                                        $mid = $mid_name['id'];
                                        $jungle = $jungle_name['id'];
                                        $top = $top_name['id'];
                                        include("evaluer_compo_user.php");
                               echo "</table>
                                </div>"; 
                    }
            }
        ?>
    
        <div align="center">
            <?php
                echo "<a href=\"index.php?id=".$_SESSION['id']."\">"?>
                    <button id="new_comp">Nouvelle Compo</button>
                </a>
            
        </div>
        <div id="new_comp_container"></div>
        <script src="index.js"></script>
    </body>
</html>
<?php
    }else{
        include("sidenav.php"); ?>
        <!DOCTYPE html>
        <html lang="fr">
            <head>
                <meta charset="utf-8" />
                <title>Compos builder</title> 
                <link rel="stylesheet" href="index.css"> 
            </head>
  
            <body id="main">

                <h1>Compos builder</h1>
                <div>
                    <p>Vous n'avez pas encore de compo enregistré.</br>Pour enregistrer une compo il vous suffit de vous inscrire et vous connecter en cliquant sur "S'inscrire" dans le panneau de navigation</p>
                </div>

                <script src="index.js"></script>
            </body>
        </html>

<?php
    }
?>