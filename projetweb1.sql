-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 29 mai 2020 à 10:19
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projetweb1`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `user_id` int(11) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  PRIMARY KEY (`pseudo`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`user_id`, `pseudo`) VALUES
(3, 'admin'),
(10, 'titi2');

-- --------------------------------------------------------

--
-- Structure de la table `composition`
--

DROP TABLE IF EXISTS `composition`;
CREATE TABLE IF NOT EXISTS `composition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `top` int(11) NOT NULL,
  `jungle` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `adc` int(11) NOT NULL,
  `supp` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `composition`
--

INSERT INTO `composition` (`id`, `top`, `jungle`, `mid`, `adc`, `supp`, `user_id`) VALUES
(2, 41, 28, 19, 25, 13, 3),
(6, 18, 28, 11, 21, 13, 7),
(7, 31, 27, 19, 21, 13, 7),
(10, 22, 45, 63, 17, 5, 12);

-- --------------------------------------------------------

--
-- Structure de la table `personnage`
--

DROP TABLE IF EXISTS `personnage`;
CREATE TABLE IF NOT EXISTS `personnage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `role` varchar(15) NOT NULL DEFAULT 'mid',
  `ap` int(11) NOT NULL,
  `ad` int(11) NOT NULL,
  `tank` int(11) NOT NULL,
  `controle` int(11) NOT NULL,
  `depush` int(11) NOT NULL,
  `early` int(11) NOT NULL,
  `midgame` int(11) NOT NULL,
  `late` int(11) NOT NULL,
  `mobility` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `NOM` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=149 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `personnage`
--

INSERT INTO `personnage` (`id`, `name`, `role`, `ap`, `ad`, `tank`, `controle`, `depush`, `early`, `midgame`, `late`, `mobility`) VALUES
(1, 'Ryze', 'top', 4, 1, 3, 3, 5, 2, 3, 5, 3),
(2, 'Aatrox', 'top', 1, 4, 3, 4, 3, 4, 3, 4, 4),
(3, 'Ahri', 'mid', 4, 1, 1, 3, 4, 3, 5, 4, 4),
(4, 'Akali', 'top', 4, 3, 2, 2, 2, 2, 4, 4, 5),
(5, 'Alistar', 'supp', 2, 1, 5, 4, 1, 4, 4, 4, 3),
(6, 'Amumu', 'jungle', 3, 1, 4, 5, 2, 3, 3, 4, 3),
(7, 'Anivia', 'mid', 5, 1, 3, 4, 4, 2, 3, 5, 2),
(8, 'Annie', 'supp', 4, 1, 2, 3, 3, 3, 4, 4, 2),
(9, 'Aphelios', 'adc', 1, 5, 1, 3, 5, 3, 4, 5, 2),
(10, 'Ashe', 'adc', 1, 5, 1, 4, 4, 4, 4, 4, 3),
(11, 'Aurelion Sol', 'mid', 4, 1, 3, 3, 3, 2, 3, 4, 3),
(12, 'Azir', 'mid', 4, 2, 2, 2, 4, 3, 4, 3, 4),
(13, 'Bard', 'supp', 2, 2, 2, 4, 1, 3, 5, 4, 4),
(14, 'Blitzcrank', 'supp', 3, 3, 3, 4, 1, 4, 4, 4, 3),
(15, 'Brand', 'supp', 4, 1, 2, 3, 2, 3, 4, 5, 2),
(16, 'Braum', 'supp', 1, 2, 5, 5, 1, 4, 4, 4, 2),
(17, 'Caitlyn', 'adc', 1, 5, 1, 3, 5, 5, 4, 4, 2),
(18, 'Camille', 'top', 1, 4, 3, 4, 3, 3, 4, 4, 4),
(19, 'Cassiopeia', 'mid', 5, 1, 2, 4, 4, 2, 4, 4, 3),
(20, 'Cho Gath', 'top', 3, 1, 4, 4, 3, 2, 3, 4, 2),
(21, 'Corki', 'adc', 3, 4, 1, 1, 4, 2, 3, 4, 4),
(22, 'Darius', 'top', 1, 4, 4, 3, 2, 3, 4, 4, 2),
(23, 'Diana', 'mid', 4, 1, 2, 3, 3, 3, 4, 4, 3),
(24, 'Dr.Mundo', 'jungle', 1, 3, 5, 2, 2, 2, 4, 5, 3),
(25, 'Draven', 'adc', 1, 5, 1, 2, 5, 4, 4, 4, 2),
(26, 'Ekko', 'jungle', 4, 2, 3, 3, 2, 3, 4, 4, 4),
(27, 'Elise', 'jungle', 4, 1, 3, 4, 2, 3, 4, 4, 4),
(28, 'Evelynn', 'jungle', 4, 1, 2, 3, 4, 2, 4, 4, 4),
(29, 'Ezreal', 'adc', 3, 4, 1, 1, 4, 3, 4, 5, 5),
(30, 'Fiddlesticks', 'jungle', 4, 1, 2, 4, 5, 4, 4, 4, 3),
(31, 'Fiora', 'top', 1, 4, 3, 3, 3, 3, 4, 4, 3),
(32, 'Fizz', 'mid', 4, 1, 2, 3, 3, 3, 4, 4, 5),
(33, 'Galio', 'mid', 4, 1, 4, 4, 3, 2, 4, 4, 5),
(34, 'Gangplank', 'top', 1, 4, 3, 2, 5, 2, 4, 5, 3),
(35, 'Garen', 'top', 1, 4, 4, 1, 3, 4, 4, 3, 3),
(36, 'Gnar', 'top', 1, 4, 4, 4, 2, 2, 4, 4, 3),
(37, 'Gragas', 'jungle', 3, 2, 4, 3, 3, 3, 4, 4, 2),
(38, 'Graves', 'jungle', 1, 5, 2, 3, 4, 3, 4, 4, 4),
(39, 'Hecarim', 'jungle', 1, 4, 3, 3, 3, 3, 5, 4, 5),
(40, 'Heimerdinger', 'top', 4, 2, 2, 3, 5, 4, 4, 3, 2),
(41, 'Illaoi', 'top', 1, 3, 4, 3, 4, 3, 4, 5, 2),
(42, 'Irelia', 'top', 1, 4, 3, 3, 3, 3, 4, 4, 3),
(43, 'Ivern', 'jungle', 3, 3, 4, 3, 1, 3, 4, 4, 3),
(44, 'Janna', 'supp', 2, 1, 2, 4, 1, 3, 4, 4, 4),
(45, 'Jarvan IV', 'jungle', 1, 4, 3, 3, 2, 3, 4, 4, 4),
(46, 'Jax', 'jungle', 1, 4, 4, 3, 3, 3, 4, 4, 4),
(47, 'Jayce', 'top', 1, 4, 2, 3, 3, 3, 4, 4, 4),
(48, 'Jhin', 'adc', 1, 5, 1, 2, 5, 4, 4, 5, 2),
(49, 'Jinx', 'adc', 1, 5, 1, 2, 5, 3, 4, 5, 3),
(50, 'Kai sa', 'adc', 3, 4, 1, 1, 5, 3, 4, 3, 2),
(51, 'Kalista', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(52, 'Karma', 'supp', 2, 2, 3, 4, 1, 2, 3, 4, 4),
(53, 'Karthus', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(54, 'Kassadin', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(55, 'Katarina', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(56, 'Kayle', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(57, 'Kayn', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(58, 'Kennen', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(59, 'Kha Zix', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(60, 'Kindred', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(61, 'Kled', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(62, 'Kog Maw', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(63, 'LeBlanc', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(64, 'Lee Sin', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(65, 'Leona', 'supp', 2, 2, 3, 4, 1, 2, 3, 4, 4),
(66, 'Lissandra', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(67, 'Lucian', 'adc', 1, 5, 1, 2, 5, 3, 4, 5, 3),
(68, 'Lulu', 'supp', 2, 2, 3, 4, 1, 2, 3, 4, 4),
(69, 'Lux', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(70, 'Maitre Yi', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(71, 'Malphite', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(72, 'Malzahar', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(73, 'Maokai', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(74, 'Miss Fortune', 'adc', 1, 5, 1, 2, 5, 3, 4, 5, 3),
(75, 'Mordekaiser', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(76, 'Morgana', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(77, 'Nami', 'supp', 2, 2, 3, 4, 1, 2, 3, 4, 4),
(78, 'Nasus', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(79, 'Nautilus', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(80, 'Nidalee', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(81, 'Neeko', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(82, 'Nocturne', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(83, 'Nunu', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(84, 'Olaf', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(85, 'Orianna', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(86, 'Ornn', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(87, 'Pantheon', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(88, 'Poppy', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(89, 'Pyke', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(90, 'Qiyana', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(91, 'Quinn', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(92, 'Rakan', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(93, 'Rammus', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(94, 'Rek Sai', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(95, 'Renekton', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(96, 'Rengar', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(97, 'Riven', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(98, 'Rumble', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(99, 'Sejuani', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(100, 'Senna', 'adc', 1, 5, 1, 2, 5, 3, 4, 5, 3),
(101, 'Sett', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(102, 'Shaco', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(103, 'Shen', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(104, 'Shyvana', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(105, 'Singed', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(106, 'Sion', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(107, 'Sivir', 'adc', 1, 5, 1, 2, 5, 3, 3, 5, 4),
(108, 'Skarner', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(109, 'Sona', 'supp', 2, 2, 3, 4, 1, 2, 3, 4, 4),
(110, 'Soraka', 'supp', 2, 2, 3, 4, 1, 2, 3, 4, 4),
(111, 'Swain', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(112, 'Sylas', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(113, 'Syndra', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(114, 'Tahm Kench', 'supp', 2, 2, 3, 4, 1, 2, 3, 4, 4),
(115, 'Taliyah', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(116, 'Talon', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(117, 'Taric', 'supp', 2, 2, 3, 4, 1, 2, 3, 4, 4),
(118, 'Teemo', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(119, 'Thresh', 'supp', 2, 2, 3, 4, 1, 2, 3, 4, 4),
(120, 'Tristana', 'adc', 1, 5, 1, 2, 5, 3, 4, 5, 3),
(121, 'Trundle', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(122, 'Tryndamere', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(123, 'Twisted Fate', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(124, 'Twitch', 'adc', 1, 5, 1, 2, 5, 3, 4, 5, 3),
(125, 'Udyr', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(126, 'Urgot', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(127, 'Varus', 'adc', 1, 5, 1, 2, 5, 3, 4, 5, 3),
(128, 'Vayne', 'adc', 1, 5, 1, 2, 5, 3, 4, 5, 3),
(129, 'Veigar', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(130, 'Vel Koz', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(131, 'Vi', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(132, 'Viktor', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(133, 'Vladimir', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(134, 'Volibear', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(135, 'Warwick', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(136, 'Wukong', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(137, 'Xayah', 'adc', 1, 5, 1, 2, 5, 3, 4, 5, 3),
(138, 'Xerath', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(139, 'Xin Zhao', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(140, 'Yasuo', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(141, 'Yorick', 'top', 2, 2, 3, 2, 3, 3, 3, 2, 2),
(142, 'Yuumi', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(143, 'Zac', 'jungle', 3, 3, 3, 3, 3, 3, 3, 3, 3),
(144, 'Zed', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(145, 'Ziggs', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(146, 'Zilean', 'supp', 2, 2, 3, 4, 1, 2, 3, 4, 4),
(147, 'Zoe', 'mid', 4, 1, 1, 4, 4, 3, 4, 3, 2),
(148, 'Zyra', 'supp', 2, 2, 3, 4, 1, 2, 3, 4, 4);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `img_profile` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `pseudo`, `password`, `email`, `img_profile`) VALUES
(12, 'testrendu', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'test@rendu.fr', 'default_profile.png'),
(11, 'test_util', '9cf95dacd226dcf43da376cdb6cbba7035218921', 'test_util@test.com', 'default_profile.png'),
(10, 'titi2', '5a79f96745b9ed94cbc0732bdb05bac33042b3e2', 'titi@minet.fr', 'default_profile.png'),
(3, 'admin', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'admin@test.fr', 'default_profile.png');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
