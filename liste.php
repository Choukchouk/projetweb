<?php
    session_start();

    include("connexiondb.php"); 
     
    if(isset($_GET['id']) AND $_GET['id'] > 0) {
       $get_id = intval($_GET['id']);
       $req_user = $dbh->prepare('SELECT * FROM user WHERE id = ?');
       $req_user->execute(array($get_id));
       $user_infos = $req_user->fetch();
    }

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Compos builder</title> 
        <link rel="stylesheet" href="index.css"> 
    </head>
  
    <body id="main">
    
        <h1>Compos builder</h1>

        <?php include("sidenav.php"); ?>

        <div id="champs_container">

        <!-- Affichage de la liste de tous les champions -->
        <?php
            foreach($dbh->query('SELECT name, id FROM personnage') as $row) {
                if(isset($_GET['id']) AND $_GET['id'] > 0) {
                        echo "<a href='champstats.php?name=".$row['name']."&id=".$_SESSION['id']."' >
                                    <img class='liste_champs_img' src='image_champs/".$row['name'].".jpg'> 
                                    <div class='champs_list_name' align='center'>
                                        ".$row['name']." 
                                    </div>
                                </a>";
                    }else{
                        echo "<a href='champstats.php?name=".$row['name']."' >
                                    <img class='liste_champs_img' src='image_champs/".$row['name'].".jpg'> 
                                    <div class='champs_list_name' align='center'>
                                        ".$row['name']." 
                                    </div>
                                </a>";
                }
            }
        ?>
        </div>

        <script src="index.js"></script>
    </body>
</html>


    