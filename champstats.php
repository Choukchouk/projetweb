<?php
    session_start();

    include("connexiondb.php"); 

    $nom_champion = $_GET['name'];
    $req_stats = $dbh->prepare('SELECT id, name, role, ap, ad, tank, controle, depush, early, midgame, late, mobility FROM personnage WHERE name = ? ');
    $req_stats->execute(array($nom_champion));
    $infos = $req_stats->fetch();

    if(isset($_GET['id']) AND $_GET['id'] == $_SESSION['id']) {
        $req_admin = $dbh->prepare("SELECT * FROM admin WHERE user_id = ?");
        $req_admin->execute(array($_SESSION['id']));
        $user_admin = $req_admin->rowCount();
    }

    $id_champ = $infos['id'];

    if(isset($_POST['edit_stats'])){

        $name = $_POST['edit_name'];
        $role = $_POST['edit_role'];
        $ap = $_POST['edit_ap'];
        $ad = $_POST['edit_ad'];
        $tank = $_POST['edit_tank'];
        $controle = $_POST['edit_controle'];
        $depush = $_POST['edit_depush'];
        $early = $_POST['edit_early'];
        $midgame = $_POST['edit_midgame'];
        $late = $_POST['edit_late'];
        $mobility = $_POST['edit_mobility'];

        $name_value = $_POST['name_value'];
        $role_value = $_POST['role_value'];
        $ap_value = $_POST['ap_value'];
        $ad_value = $_POST['ad_value'];
        $tank_value = $_POST['tank_value'];
        $controle_value = $_POST['controle_value'];
        $depush_value = $_POST['depush_value'];
        $early_value = $_POST['early_value'];
        $midgame_value = $_POST['midgame_value'];
        $late_value = $_POST['late_value'];
        $mobility_value = $_POST['mobility_value'];

        if($name != $name_value){
            $req_name = $dbh->prepare('UPDATE personnage SET name = ? WHERE id = ? ');
            $req_name->execute(array($name, $id_champ));
        }
        if($role != $role_value){
            $req_role = $dbh->prepare('UPDATE personnage SET role = ? WHERE id = ? ');
            $req_role->execute(array($role, $id_champ));
        }
        if($ap != $ap_value){
            $req_ap = $dbh->prepare('UPDATE personnage SET ap = ? WHERE id = ? ');
            $req_ap->execute(array($ap, $id_champ));
        }
        if($ad != $ad_value){
            $req_ad = $dbh->prepare('UPDATE personnage SET ad = ? WHERE id = ? ');
            $req_ad->execute(array($ad, $id_champ));
        }
        if($tank != $tank_value){
            $req_tank = $dbh->prepare('UPDATE personnage SET tank = ? WHERE id = ? ');
            $req_tank->execute(array($tank, $id_champ));
        }
        if($controle != $controle_value){
            $req_controle = $dbh->prepare('UPDATE personnage SET controle = ? WHERE id = ? ');
            $req_controle->execute(array($controle, $id_champ));
        }
        if($early != $early_value){
            $req_early = $dbh->prepare('UPDATE personnage SET early = ? WHERE id = ? ');
            $req_early->execute(array($early, $id_champ));
        }
        if($midgame != $midgame_value){
            $req_midgame = $dbh->prepare('UPDATE personnage SET midgame = ? WHERE id = ? ');
            $req_midgame->execute(array($midgame, $id_champ));
        }
        if($late != $late_value){
            $req_late = $dbh->prepare('UPDATE personnage SET late = ? WHERE id = ? ');
            $req_late->execute(array($late, $id_champ));
        }
        if($depush != $depush_value){
            $req_depush = $dbh->prepare('UPDATE personnage SET depush = ? WHERE id = ? ');
            $req_depush->execute(array($depush, $id_champ));
        }
        if($mobility != $mobility_value){
            $req_mobility = $dbh->prepare('UPDATE personnage SET mobility = ? WHERE id = ? ');
            $req_mobility->execute(array($mobility, $id_champ));
        }
        if(isset($_GET['id']) AND $_GET['id'] > 0) {
            header("Location: champstats.php?name=".$name."&id=".$_SESSION['id']);
        }else{
            header("Location: champstats.php?name=".$name);
        }
    }

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Compos builder</title> 
        <link rel="stylesheet" href="index.css"> 
    </head>
  
    <body id="main">
    
        <h1>Compos builder</h1>

        <?php include("sidenav.php"); ?>

        

        <div class="infos_container">
            <table class="champs_infos_table">
                <tr>
                    <td id="img_td" rowspan="11">
                        <?php echo "<img class='champions_infos_img' src='image_champs/".$nom_champion.".jpg'>"; ?>
                    </td>
                    <td class="left">
                        Nom
                    </td>
                    <td>
                        <?php echo $infos['name'] ;?>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        Rôle
                    </td>
                    <td>
                        <?php echo $infos['role'] ;?>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        AP
                    </td>
                    <td>
                        <?php echo $infos['ap'] ;?>     
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        AD
                    </td>
                    <td>
                        <?php echo $infos['ad'] ;?>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        Tanking
                    </td>
                    <td>
                        <?php echo $infos['tank'] ;?>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        Contrôle
                    </td>
                    <td>
                        <?php echo $infos['controle'] ;?>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        Depush
                    </td>
                    <td>
                        <?php echo $infos['depush'] ;?>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        Early game
                    </td>
                    <td>
                        <?php echo $infos['early'] ;?>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        Mid game
                    </td>
                    <td>
                        <?php echo $infos['midgame'] ;?>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        Late game
                    </td>
                    <td>
                        <?php echo $infos['late'] ;?>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        Mobility
                    </td>
                    <td>
                        <?php echo $infos['mobility'] ;?>
                    </td>
                </tr>    
            </table>

            <!-- Possibilitée d'éditer les stats si l'on est administrateur -->
            <?php if($user_admin == 1) { 
                    echo "<form method='POST' action=''>
                            <table>
                                <tr>
                                    <td>
                                        Nom
                                    </td>
                                    <td>
                                        <input type='hidden' name='name_value' value=".$infos['name']."> 
                                    </td>
                                    <td>    
                                        <input type='text' placeholder='Edit name' name='edit_name' value='".$infos['name']."'>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Rôle
                                    </td>
                                    <td>
                                        <input type='hidden' name='role_value' value=".$infos['role']."> 
                                    </td>
                                    <td>    
                                        <input type='text' placeholder='Edit role' name='edit_role' value='".$infos['role']."'>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        AP
                                    </td>
                                    <td>
                                        <input type='hidden' name='ap_value' value=".$infos['ap'].">    
                                    </td>
                                    <td>    
                                        <input type='text' placeholder='Edit ap' name='edit_ap' value='".$infos['ap']."'>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        AD
                                    </td>
                                    <td>
                                        <input type='hidden' name='ad_value' value=".$infos['ad']."> 
                                    </td>
                                    <td>    
                                        <input type='text' placeholder='Edit ad' name='edit_ad' value='".$infos['ad']."'>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tanking
                                    </td>
                                    <td>
                                        <input type='hidden' name='tank_value' value=".$infos['tank'].">
                                    </td>
                                    <td>    
                                        <input type='text' placeholder='Edit tank' name='edit_tank' value='".$infos['tank']."'>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Contrôle
                                    </td>
                                    <td>
                                        <input type='hidden' name='controle_value' value=".$infos['controle']."> 
                                    </td>
                                    <td>    
                                        <input type='text' placeholder='Edit controle' name='edit_controle' value='".$infos['controle']."'>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Depush
                                    </td>
                                    <td>
                                        <input type='hidden' name='depush_value' value=".$infos['depush'].">
                                    </td>
                                    <td>    
                                        <input type='text' placeholder='Edit depush' name='edit_depush' value='".$infos['depush']."'>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Early game
                                    </td>
                                    <td>
                                        <input type='hidden' name='early_value' value=".$infos['early']."> 
                                    </td>
                                    <td>    
                                        <input type='text' placeholder='Edit early' name='edit_early' value='".$infos['early']."'>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Mid game
                                    </td>
                                    <td>
                                        <input type='hidden' name='midgame_value' value=".$infos['midgame'].">
                                    </td>
                                    <td>    
                                        <input type='text' placeholder='Edit midgame' name='edit_midgame' value='".$infos['midgame']."'>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Late game
                                    </td>
                                    <td>
                                        <input type='hidden' name='late_value' value=".$infos['late'].">
                                    </td>
                                    <td>    
                                        <input type='text' placeholder='Edit late' name='edit_late' value='".$infos['late']."'>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Mobility
                                    </td>
                                    <td>
                                        <input type='hidden' name='mobility_value' value=".$infos['mobility'].">
                                    </td>
                                    <td>    
                                        <input type='text' placeholder='Edit mobility' name='edit_mobility' value='".$infos['mobility']."'>
                                    </td>
                                </tr>
                            </table>
                            <button type='submit' name='edit_stats' class=''>Edit stats</button>
                        </form>";
                }
            ?>
        </div>

        <script src="index.js"></script>
    </body>
</html>

        