<?php
	 
	include("connexiondb.php");

    $user_admin = 0;

    if(isset($_GET['id']) AND $_GET['id'] == $_SESSION['id']) {
        $req_infos = $dbh->prepare("SELECT * FROM user WHERE id = ?");
        $req_infos->execute(array($_GET['id']));
        $user_infos_img = $req_infos->fetch();
        $req_admin = $dbh->prepare("SELECT * FROM admin WHERE user_id = ?");
        $req_admin->execute(array($_SESSION['id']));
        $user_admin = $req_admin->rowCount();
    }	 
	 
	if(isset($_POST['identification'])) {
	   $mailconnect = htmlspecialchars($_POST['mailconnect']);
	   $mdpconnect = ($_POST['mdpconnect']);
	   if(!empty($mailconnect) AND !empty($mdpconnect)) {
            $mdpconnect = sha1($mdpconnect);
	        $req_user = $dbh->prepare("SELECT * FROM user WHERE email = ? AND password = ?");
	        $req_user->execute(array($mailconnect, $mdpconnect));
	        $user_exist = $req_user->rowCount();
	        if($user_exist == 1) {
	            $user_infos = $req_user->fetch();
	            $_SESSION['id'] = $user_infos['id'];
	            $_SESSION['pseudo'] = $user_infos['pseudo'];
	            $_SESSION['mail'] = $user_infos['email'];
	            header("Location: mescompos.php?id=".$_SESSION['id']);
	        } else {
	            $erreur = "Mauvais mail ou mot de passe !";
	        }
	    } else {
	       $erreur = "Tous les champs doivent être complétés !";
	    }
	}

?>

        
    <input type="image" id="navButton" alt="open_navigation" onclick="openNav()"
    	width='50px' height='50px' src="menu.png">

    <div id="mySidenav" class="sidenav">
      <a href="#" class="closebtn" onclick="closeNav()"; return false;>&times;</a> 
      <?php if(isset($_GET['id']) AND $_GET['id'] == $_SESSION['id']) { 
                        echo "<a href=\"userprofil.php?id=".$_SESSION['id']."\">
                                    <img src='image_profiles/".$user_infos_img['img_profile']."'  style='width:60px; height:60px;'>
                              </a>";
                  }
      ?>
      <?php if(isset($_GET['id']) AND $_GET['id'] == $_SESSION['id']) { 
      			echo "<a href=\"index.php?id=".$_SESSION['id']."\">Accueil</a>";
      		}else{
      			echo "<a href='index.php'>Accueil</a>";
      		}
      ?>
      <?php if(isset($_GET['id']) AND $_GET['id'] == $_SESSION['id']) { 
      			echo "<a href=\"mescompos.php?id=".$_SESSION['id']."\">Mes compos</a>";
      		}else{
      			echo "<a href='mescompos.php'>Mes compos</a>";
      		}
      ?>
      <?php if(isset($_GET['id']) AND $_GET['id'] == $_SESSION['id']) { 
      			echo "<a href=\"liste.php?id=".$_SESSION['id']."\">Les champions</a>";
      		}else{
      			echo "<a href='liste.php'>Les champions</a>";
      		}
      ?>
        <?php
            if($user_admin == 1) {
                echo "<a href=\"admin.php?id=".$_SESSION['id']."\">Gérer les admins</a>";
            }
        ?>
      <?php if(isset($_GET['id']) AND $_GET['id'] == $_SESSION['id']) { 
      			echo "<a href=\"enregistrer.php?id=".$_SESSION['id']."\">S'inscrire</a>";
      		}else{
      			echo "<a href='enregistrer.php'>S'inscrire</a>";
      		}
      ?>
      <?php if(!isset($_GET['id'])) { 
                echo "<form method='POST' action=''>
                        <input type='text' placeholder='Email' id='mailconnect' name='mailconnect'>
                        <input type='password' placeholder='Mot de passe' id='mdpconnect' name='mdpconnect'>
                        <button type='submit' name='identification' class='signin'>S'identifier</button>
                    </form>";
            }
      ?>
      	<?php if(isset($_GET['id']) AND $_GET['id'] == $_SESSION['id']) {
      			echo "<a href='deconnexion.php' class='button'>Déconnexion</a>";
      		}
      	?>
    </div>

        