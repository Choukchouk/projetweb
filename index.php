<?php
    session_start();

    include("connexiondb.php"); 


?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Compos builder</title> 
	    <link rel="stylesheet" href="index.css"> 
    </head>
  
    <body id="main">
    
        <h1>Compos builder</h1>

        <?php include("sidenav.php"); ?>

        <h1>Entrez votre compo :</h1>
  		
  		<?php include("composelect.php"); ?>

        <div id="conseil">

        </div>
        
        <script
			  src="https://code.jquery.com/jquery-3.5.0.min.js"
			  integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
			  crossorigin="anonymous"></script>
        <script src="index.js"></script>
    </body>
</html>
