<?php
   session_start();

	include("connexiondb.php"); 

	if(isset($_POST['inscription'])) {
	    $pseudo = htmlspecialchars($_POST['pseudo']);
	    $mail = htmlspecialchars($_POST['mail']);
	    $mail2 = htmlspecialchars($_POST['mail2']);
	    $mdp = $_POST['mdp'];
	    $mdp2 = $_POST['mdp2'];
        $img_profil_default = "default_profile.png";
	    if(!empty($_POST['pseudo']) AND !empty($_POST['mail']) AND !empty($_POST['mail2']) AND !empty($_POST['mdp']) AND !empty($_POST['mdp2'])) {
	        $pseudolength = strlen($pseudo);
	        if($pseudolength <= 30) {
	            if($mail == $mail2) {
                    /*Vérification de la conformitée de l'adresse email*/
	                if(filter_var($mail, FILTER_VALIDATE_EMAIL)) {
	                   $req_mail = $dbh->prepare("SELECT * FROM user WHERE email = ?");
	                   $req_mail->execute(array($mail));
	                   $nb_mail = $req_mail->rowCount();
	                   if($nb_mail == 0) {
                            $mdp = sha1($_POST['mdp']);
                            $mdp2 = sha1($_POST['mdp2']);
	                        if($mdp == $mdp2) {
	                            $insert_user = $dbh->prepare("INSERT INTO user (pseudo, password, email, img_profile) VALUES(?, ?, ?, ?)");
	                    	    $insert_user->execute(array($pseudo, $mdp, $mail, $img_profil_default));
	                    	    header("Location: index.php"); 
	                        } else {
	                     	    $erreur = "Vos mots de passes ne correspondent pas !";
	                        }
	                    } else {
	                  	    $erreur = "Adresse mail déjà utilisée !";
	                    }
	                } else {
	               	    $erreur = "Votre adresse mail n'est pas valide !";
	                }
	            } else {
	            	$erreur = "Vos adresses mail ne correspondent pas !";
	            }
	        } else {
	           $erreur = "Votre pseudo ne doit pas dépasser 30 caractères !";
	        }
	    } else {
	        $erreur = "Tous les champs doivent être complétés !";
	    }
	}
?>


<html>
   <head>
      <title>TUTO PHP</title>
      <meta charset="utf-8">
      <link rel="stylesheet" href="index.css"> 
   </head>
   <body>
   	<?php include("sidenav.php"); ?>
      <div id="form_inscription_div" align="center">
            <h1>Inscription</h1>
            <br /><br />
            <form method="POST" action="">
                <table>
                    <tr>
                        <td align="right">
                            <label for="pseudo">Pseudo :</label>
                        </td>
                        <td>
                            <input type="text" placeholder="Votre pseudo" id="pseudo" name="pseudo" value="<?php if(isset($pseudo)) { echo $pseudo; } ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <label for="mail">Mail :</label>
                        </td>
                        <td>
                            <input type="email" placeholder="Votre mail" id="mail" name="mail" value="<?php if(isset($mail)) { echo $mail; } ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <label for="mail2">Confirmation du mail :</label>
                        </td>
                        <td>
                            <input type="email" placeholder="Confirmez votre mail" id="mail2" name="mail2" value="<?php if(isset($mail2)) { echo $mail2; } ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <label for="mdp">Mot de passe :</label>
                        </td>
                        <td>
                            <input type="password" placeholder="Votre mot de passe" id="mdp" name="mdp" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <label for="mdp2">Confirmation du mot de passe :</label>
                        </td>
                        <td>
                            <input type="password" placeholder="Confirmez votre mot de passe" id="mdp2" name="mdp2" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="center">
                            <br />
                            <input type="submit" name="inscription" value="Inscription" />
                        </td>
                    </tr>
                </table>
            </form>
        <?php
            if(isset($erreur)) {
                echo '<font color="red">'.$erreur."</font>";
            }
        ?>
      </div>
      <script src="index.js"></script>
   </body>
</html>