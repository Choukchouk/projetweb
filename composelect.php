<?php 
	include("connexiondb.php"); 

	if(isset($_GET['id']) AND $_GET['id'] > 0) {
		if(isset($_POST['new_compo'])){
			$new_supp = $_POST['new_supp'];
			$new_adc = $_POST['new_adc'];
			$new_mid = $_POST['new_mid'];
			$new_jungle = $_POST['new_jungle'];
			$new_top = $_POST['new_top'];
			$user_id = $_GET['id'];
			if(($new_supp != '0') AND ($new_adc != '0') AND ($new_mid != '0') AND ($new_jungle != '0') AND ($new_top != '0')){
				$insert_compo = $dbh->prepare("INSERT INTO composition (supp, adc, mid, jungle, top, user_id) VALUES(?, ?, ?, ?, ?, ?)");
	            $insert_compo->execute(array($new_supp, $new_adc, $new_mid, $new_jungle, $new_top, $user_id));
			}
			else{
				$erreur = "Tous les postes doivent être sélectionnés";
			}
		}
	}
?>

		<div id="img_container">
        	<img id="supp_img" class="select_img" src="fond_blanc.png">
        	<img id="adc_img" class="select_img" src="fond_blanc.png">
        	<img id="mid_img" class="select_img" src="fond_blanc.png">
        	<img id="jungle_img" class="select_img" src="fond_blanc.png">
        	<img id="top_img" class="select_img" src="fond_blanc.png">
        </div>

        <form method="POST" action="">       
	  		<nav>
				<select id="supp" name='new_supp'  class="select-position" onchange="changeImgSupp()">
					<option value="0" >Support</option>
				<?php
					foreach($dbh->query('SELECT name,id from personnage WHERE role="supp"') as $row) {
						echo  " <option value='" . $row['id'] . "'>". $row['name'] ." </option>"; }
				?>
				</select>
				<select id="adc" name='new_adc' class="select-position" onchange="changeImgAdc()">
					<option value="0" >Adc </option>
				<?php		
					foreach($dbh->query('SELECT name,id from personnage WHERE role="adc"') as $row) {
						echo  " <option value='" . $row['id'] . "'>". $row['name'] ."</option>"; }
				?>
				</select>
				<select id="mid" name='new_mid' class="select-position" onchange="changeImgMid()">
					<option value="0" >Mid</option>
				<?php
					foreach($dbh->query('SELECT name,id from personnage WHERE role="mid"') as $row) {
						echo  " <option value='" . $row['id'] . "'>". $row['name'] ."</option>"; }
				?>
				</select>
				<select id="jungle" name='new_jungle' class="select-position" onchange="changeImgJungle()">
					<option value="0" >Jungle</option>
				<?php
					foreach($dbh->query('SELECT name,id from personnage WHERE role="jungle"') as $row) {
						echo  " <option value='" . $row['id'] . "'>". $row['name'] ."</option>"; }
				?>
				</select>
				<select id="top" name='new_top' class="select-position" onchange="changeImgTop()">
					<option value="0" >Top</option>
				<?php
					foreach($dbh->query('SELECT name,id from personnage WHERE role="top"') as $row) {
						echo  " <option value='" . $row['id'] . "'>". $row['name'] ."</option>"; }
				?>
				</select>
			</nav>
			<?php
				if(isset($_GET['id']) AND $_GET['id'] > 0) {
					echo "<input type='submit' name='new_compo' value='Ajouter cette compo' />"; 
				}
			?>      
   		</form>
   		<?php
         if(isset($erreur)) {
            echo '<font color="red">'.$erreur."</font>";
         }
         ?>

        