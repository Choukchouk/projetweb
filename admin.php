<?php
    session_start();

    include("connexiondb.php"); 

    if(isset($_GET['id']) AND $_GET['id'] == $_SESSION['id']) {
        $req_admin = $dbh->prepare("SELECT * FROM admin WHERE user_id = ?");
    }

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Compos builder</title> 
	    <link rel="stylesheet" href="index.css"> 
    </head>
  
    <body id="main">
        <?php include("sidenav.php"); ?>

        <h1>Ajout et suppression des administrateurs :</h1>
  		
        <form method='POST' action=''>
            <?php
                foreach($dbh->query('SELECT pseudo,id from user') as $row) {
                    $req_admin->execute(array($row['id']));
                    $user_admin = $req_admin->rowCount();

                    echo  " <p class='admins'> ". $row['pseudo'] ."";
                                if($user_admin == 0){
                                    echo "<a href=\"ajout_admin.php?pseudo=".$row['pseudo']."&id=".$_SESSION['id']."&ajout_id=".$row['id']."\">
                                        <img src='add.png' class='edit_icons'>
                                    </a></p>";
                                }
                                if($user_admin == 1){
                                    echo"<a href=\"delete_admin.php?id=".$_SESSION['id']."&delete_id=".$row['id']."\">
                                            <img src='delete.png' class='edit_icons'>
                                        </a></p>"; 
                                }
                    }
            ?>
        </form>

        <script src="index.js"></script>
    </body>
</html>
