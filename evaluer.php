<?php 
	include("connexiondb.php");

	$supp_id = $_GET["supp"];
	$adc_id = $_GET["adc"]; 
	$mid_id = $_GET["mid"]; 
	$jungle_id = $_GET["jungle"]; 
	$top_id = $_GET["top"]; 

	
	$supp_fct = $dbh->prepare("SELECT * FROM personnage WHERE id = :id"); 
	$supp_fct->execute(array(':id' => $supp_id)); 
	$row = $supp_fct->fetch();
	$supp_name = $row['name'];
	$supp_midgame = $row['midgame'];
	$supp_late = $row['late'];
	$supp_early = $row['early'];
	$supp_ap = $row['ap'];
	$supp_ad = $row['ad'];
	$supp_tank = $row['tank'];
	$supp_controle = $row['controle'];
	$supp_depush = $row['depush'];
	$supp_mobility = $row['mobility'];

	$adc_fct = $dbh->prepare("SELECT * FROM personnage WHERE id = :id"); 
	$adc_fct->execute(array(':id' => $adc_id)); 
	$row = $adc_fct->fetch();
	$adc_name = $row['name'];
	$adc_midgame = $row['midgame'];
	$adc_late = $row['late'];
	$adc_early = $row['early'];
	$adc_ap = $row['ap'];
	$adc_ad = $row['ad'];
	$adc_tank = $row['tank'];
	$adc_controle = $row['controle'];
	$adc_depush = $row['depush'];
	$adc_mobility = $row['mobility'];

	$mid_fct = $dbh->prepare("SELECT * FROM personnage WHERE id = :id"); 
	$mid_fct->execute(array(':id' => $mid_id)); 
	$row = $mid_fct->fetch();
	$mid_name = $row['name'];
	$mid_midgame = $row['midgame'];
	$mid_late = $row['late'];
	$mid_early = $row['early'];
	$mid_ap = $row['ap'];
	$mid_ad = $row['ad'];
	$mid_tank = $row['tank'];
	$mid_controle = $row['controle'];
	$mid_depush = $row['depush'];
	$mid_mobility = $row['mobility'];

	$jungle_fct = $dbh->prepare("SELECT * FROM personnage WHERE id = :id"); 
	$jungle_fct->execute(array(':id' => $jungle_id)); 
	$row = $jungle_fct->fetch();
	$jungle_name = $row['name'];
	$jungle_midgame = $row['midgame'];
	$jungle_late = $row['late'];
	$jungle_early = $row['early'];
	$jungle_ap = $row['ap'];
	$jungle_ad = $row['ad'];
	$jungle_tank = $row['tank'];
	$jungle_controle = $row['controle'];
	$jungle_depush = $row['depush'];
	$jungle_mobility = $row['mobility'];

	$top_fct = $dbh->prepare("SELECT * FROM personnage WHERE id = :id"); 
	$top_fct->execute(array(':id' => $top_id)); 
	$row = $top_fct->fetch();
	$top_name = $row['name'];
	$top_midgame = $row['midgame'];
	$top_late = $row['late'];
	$top_early = $row['early'];
	$top_ap = $row['ap'];
	$top_ad = $row['ad'];
	$top_tank = $row['tank'];
	$top_controle = $row['controle'];
	$top_depush = $row['depush'];
	$top_mobility = $row['mobility'];

	$team_ad = $top_ad + $mid_ad +$adc_ad + $jungle_ad + $supp_ad ;
	$team_ap = $top_ap + $mid_ap +$adc_ap + $jungle_ap + $supp_ap ;
	$team_tank = $top_tank + $mid_tank + $adc_tank + $supp_tank + $jungle_tank ;
	$team_controle = $top_controle + $mid_controle + $adc_controle + $supp_controle + $jungle_controle ;
	$team_depush = $top_depush + $mid_depush + $adc_depush + $supp_depush + $jungle_depush;
	$team_late = $top_late + $mid_late + $adc_late + $supp_late + $jungle_late;
	$team_midgame = $top_midgame + $mid_midgame + $adc_midgame + $supp_midgame + $jungle_midgame;
	

	/*Points négatifs*/
	echo "<table class='conseils_table'><tr><th id='negatif_head'><img class='conseils_tab_img' src='negatif.png'>Points négatifs</th><th id='positif_head'><img class='conseils_tab_img' src='positif.png'>Points positifs</th><th id='conseil_head'><img class='conseils_tab_img' src='conseils.png'>Conseils</th></tr><tr><td>";
	if($team_ap < 9 ){
		echo "<img class='conseils_query_img' src='bad.png'>Votre équipe manque de dégats AP </br>";
	}
	if($team_ad<11){
		echo "<img class='conseils_query_img' src='bad.png'>Votre équipe manque de dégats AD </br>";
	}
	if($team_tank<20){
		echo "<img class='conseils_query_img' src='bad.png'>Votre équipe manque de tankiness </br>";
	}
	if($team_controle<12){
		echo "<img class='conseils_query_img' src='bad.png'>Votre équipe manque de controles </br>";
	}
	if($adc_late<5 && $mid_late<5){
		echo "<img class='conseils_query_img' src='bad.png'>Vos deux carry mid et adc ne sont pas très fort en fin de partie </br>";
	}
	if($team_late<22){
		echo"<img class='conseils_query_img' src='bad.png'>Votre équipe n'est très forte en toute fin de partie </br>";
	}
	if($team_midgame<20){
		echo"<img class='conseils_query_img' src='bad.png'>Votre équipe n'est très forte en milieu de partie </br>";
	}
	echo "</td>";



	
	/*Points positifs*/
	echo "<td>";
	if($adc_early+$supp_early>6){
		echo"<img class='conseils_query_img' src='good.png'>Votre botlane a toute ses chances en début de partie </br>";
	}
	if($mid_early+$jungle_early>6){
		echo"<img class='conseils_query_img' src='good.png'>Votre duo mid/jungle est fort en early, vous pouvez vous appuyer dessus </br>";
	}
	if($top_early+$jungle_early>6){
		echo"<img class='conseils_query_img' src='good.png'>Votre duo top/jungle est fort en early, vous pouvez vous appuyer dessus </br>";
	}
	if($jungle_late>=4){
		echo"<img class='conseils_query_img' src='good.png'>Votre jungle est fort en fin de partie, laiisez le farm et jouez safe! </br>";
	}
	if($mid_mobility>=4){
		echo"<img class='conseils_query_img' src='good.png'>Votre mid laner est mobile, vous pouvez jouer les sidelanes avec lui en soutiens </br>";
	}
	echo "</td>";



	
	/*Conseils*/
	echo "<td>";
	if($supp_mobility>3){
		echo "<img class='conseils_query_img' src='correct.png'>$supp_name doit penser à décaler sur la map, il dispose d'une bonne mobilité </br>";
	}
	if($team_depush<2){
		echo "<img class='conseils_query_img' src='correct.png'>Si vous subbissez les assauts sur plusieurs lignes en fin de partie, vous aurez du mal à controler toute les voies à cause de votre faible capacité à depush </br>";
	}
	if($top_mobility+$top_controle<6){
		echo "<img class='conseils_query_img' src='correct.png'>$top_name risque d'être vulnérables aux ganks ennemis s'il est mal positionné </br>";
	}
	if($mid_mobility+$mid_controle<6){
		echo "<img class='conseils_query_img' src='correct.png'>$mid_name risque d'être vulnérables aux ganks ennemis s'il est mal positionné </br>";
	}
	if($mid_controle>2 && $mid_mobility>2){
		echo "<img class='conseils_query_img' src='correct.png'>$jungle_name pourra facilement gank le mid grâce aux controles et à la mobilité de $mid_name </br>" ;
	}
	if($adc_mobility+$supp_controle<6){
		echo "<img class='conseils_query_img' src='correct.png'>Attentions aux gank sur votre bot lane, $adc_name y sera vulnérable car $supp_name manque de controle </br>";
	}
	echo "</td></tr></table>";



















?>
