/* Fonction JQuery qui affiche dynamiquement le tableau des conseils lorsque qu'une composition est entièrement sélectionnée */
$(function() {
  /*$("#top").val("4")  ;*/
  $( ".select-position" ).on( "change", function() {
    var supp = $("#supp").val();
    var adc = $("#adc").val();
    var mid = $("#mid").val();
    var top = $("#top").val();
    var jungle = $("#jungle").val();
if(supp !="0" && adc !="0" && top !="0" && jungle !="0" && mid !="0" ){
  $.ajax({
  url: "evaluer.php",
  data: {
   supp: supp,
   adc: adc,
   mid: mid,
   jungle: jungle,
   top: top,

  },
  success: function( result ){
    conseil.innerHTML=result
  }

});
}
});
});


/* 'Pousse' le document de 250 px pour afficher la barre de navigation*/
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    document.getElementById("navButton").style.marginLeft = "-250px";
    document.getElementById("navButton").style.transitionDuration = "0.5s";
}

/* Ferme la barre de navigation */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    document.getElementById("navButton").style.marginLeft = "0";
    document.getElementById("navButton").style.transitionDuration = "0.5s";
}

/* Fonction d'affichage des images des supports */
function changeImgSupp(){
    var suppImg = document.getElementById("supp_img");
    var suppNameSelected = document.getElementById("supp");
    var suppName = suppNameSelected.options[suppNameSelected.selectedIndex].text;
    suppImg.setAttribute("src", "image_champs/"+suppName+".jpg");
}

/* Fonction d'affichage des images des adc */
function changeImgAdc(){
    var adcImg = document.getElementById("adc_img");
    var adcNameSelected = document.getElementById("adc");
    var adcName = adcNameSelected.options[adcNameSelected.selectedIndex].text;
    adcImg.setAttribute("src", "image_champs/"+adcName+".jpg");
}

/* Fonction d'affichage des images des mid */
function changeImgMid(){
    var midImg = document.getElementById("mid_img");
    var midNameSelected = document.getElementById("mid");
    var midName = midNameSelected.options[midNameSelected.selectedIndex].text;
    midImg.setAttribute("src", "image_champs/"+midName+".jpg");
}

/* Fonction d'affichage des images des jungle */
function changeImgJungle(){
    var jungleImg = document.getElementById("jungle_img");
    var jungleNameSelected = document.getElementById("jungle");
    var jungleName = jungleNameSelected.options[jungleNameSelected.selectedIndex].text;
    jungleImg.setAttribute("src", "image_champs/"+jungleName+".jpg");
}

/* Fonction d'affichage des images des top */
function changeImgTop(){
    var topImg = document.getElementById("top_img");
    var topNameSelected = document.getElementById("top");
    var topName = topNameSelected.options[topNameSelected.selectedIndex].text;
    topImg.setAttribute("src", "image_champs/"+topName+".jpg");
}

